/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class mapper_Map_Slot */

#ifndef _Included_mapper_Map_Slot
#define _Included_mapper_Map_Slot
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     mapper_Map_Slot
 * Method:    numProperties
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_mapper_Map_00024Slot_numProperties
  (JNIEnv *, jobject);

/*
 * Class:     mapper_Map_Slot
 * Method:    property
 * Signature: (Ljava/lang/String;)Lmapper/Value;
 */
JNIEXPORT jobject JNICALL Java_mapper_Map_00024Slot_property__Ljava_lang_String_2
  (JNIEnv *, jobject, jstring);

/*
 * Class:     mapper_Map_Slot
 * Method:    property
 * Signature: (I)Lmapper/Property;
 */
JNIEXPORT jobject JNICALL Java_mapper_Map_00024Slot_property__I
  (JNIEnv *, jobject, jint);

/*
 * Class:     mapper_Map_Slot
 * Method:    setProperty
 * Signature: (Ljava/lang/String;Lmapper/Value;Z)Lmapper/Map/Slot;
 */
JNIEXPORT jobject JNICALL Java_mapper_Map_00024Slot_setProperty
  (JNIEnv *, jobject, jstring, jobject, jboolean);

/*
 * Class:     mapper_Map_Slot
 * Method:    removeProperty
 * Signature: (Ljava/lang/String;)Lmapper/Map/Slot;
 */
JNIEXPORT jobject JNICALL Java_mapper_Map_00024Slot_removeProperty
  (JNIEnv *, jobject, jstring);

/*
 * Class:     mapper_Map_Slot
 * Method:    clearStagedProperties
 * Signature: ()Lmapper/Map/Slot;
 */
JNIEXPORT jobject JNICALL Java_mapper_Map_00024Slot_clearStagedProperties
  (JNIEnv *, jobject);

/*
 * Class:     mapper_Map_Slot
 * Method:    mapperSlotBoundMax
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL Java_mapper_Map_00024Slot_mapperSlotBoundMax
  (JNIEnv *, jobject, jlong);

/*
 * Class:     mapper_Map_Slot
 * Method:    mapperSetSlotBoundMax
 * Signature: (JI)V
 */
JNIEXPORT void JNICALL Java_mapper_Map_00024Slot_mapperSetSlotBoundMax
  (JNIEnv *, jobject, jlong, jint);

/*
 * Class:     mapper_Map_Slot
 * Method:    mapperSlotBoundMin
 * Signature: (J)I
 */
JNIEXPORT jint JNICALL Java_mapper_Map_00024Slot_mapperSlotBoundMin
  (JNIEnv *, jobject, jlong);

/*
 * Class:     mapper_Map_Slot
 * Method:    mapperSetSlotBoundMin
 * Signature: (JI)V
 */
JNIEXPORT void JNICALL Java_mapper_Map_00024Slot_mapperSetSlotBoundMin
  (JNIEnv *, jobject, jlong, jint);

/*
 * Class:     mapper_Map_Slot
 * Method:    calibrating
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_mapper_Map_00024Slot_calibrating
  (JNIEnv *, jobject);

/*
 * Class:     mapper_Map_Slot
 * Method:    setCalibrating
 * Signature: (Z)Lmapper/Map/Slot;
 */
JNIEXPORT jobject JNICALL Java_mapper_Map_00024Slot_setCalibrating
  (JNIEnv *, jobject, jboolean);

/*
 * Class:     mapper_Map_Slot
 * Method:    causesUpdate
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_mapper_Map_00024Slot_causesUpdate
  (JNIEnv *, jobject);

/*
 * Class:     mapper_Map_Slot
 * Method:    setCausesUpdate
 * Signature: (Z)Lmapper/Map/Slot;
 */
JNIEXPORT jobject JNICALL Java_mapper_Map_00024Slot_setCausesUpdate
  (JNIEnv *, jobject, jboolean);

/*
 * Class:     mapper_Map_Slot
 * Method:    maximum
 * Signature: ()Lmapper/Value;
 */
JNIEXPORT jobject JNICALL Java_mapper_Map_00024Slot_maximum
  (JNIEnv *, jobject);

/*
 * Class:     mapper_Map_Slot
 * Method:    setMaximum
 * Signature: (Lmapper/Value;)Lmapper/Map/Slot;
 */
JNIEXPORT jobject JNICALL Java_mapper_Map_00024Slot_setMaximum
  (JNIEnv *, jobject, jobject);

/*
 * Class:     mapper_Map_Slot
 * Method:    minimum
 * Signature: ()Lmapper/Value;
 */
JNIEXPORT jobject JNICALL Java_mapper_Map_00024Slot_minimum
  (JNIEnv *, jobject);

/*
 * Class:     mapper_Map_Slot
 * Method:    setMinimum
 * Signature: (Lmapper/Value;)Lmapper/Map/Slot;
 */
JNIEXPORT jobject JNICALL Java_mapper_Map_00024Slot_setMinimum
  (JNIEnv *, jobject, jobject);

/*
 * Class:     mapper_Map_Slot
 * Method:    useInstances
 * Signature: ()Z
 */
JNIEXPORT jboolean JNICALL Java_mapper_Map_00024Slot_useInstances
  (JNIEnv *, jobject);

/*
 * Class:     mapper_Map_Slot
 * Method:    setUseInstances
 * Signature: (Z)Lmapper/Map/Slot;
 */
JNIEXPORT jobject JNICALL Java_mapper_Map_00024Slot_setUseInstances
  (JNIEnv *, jobject, jboolean);

/*
 * Class:     mapper_Map_Slot
 * Method:    signal
 * Signature: ()Lmapper/Signal;
 */
JNIEXPORT jobject JNICALL Java_mapper_Map_00024Slot_signal
  (JNIEnv *, jobject);

#ifdef __cplusplus
}
#endif
#endif
