/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class mapper_Link */

#ifndef _Included_mapper_Link
#define _Included_mapper_Link
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     mapper_Link
 * Method:    numProperties
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_mapper_Link_numProperties
  (JNIEnv *, jobject);

/*
 * Class:     mapper_Link
 * Method:    property
 * Signature: (Ljava/lang/String;)Lmapper/Value;
 */
JNIEXPORT jobject JNICALL Java_mapper_Link_property__Ljava_lang_String_2
  (JNIEnv *, jobject, jstring);

/*
 * Class:     mapper_Link
 * Method:    property
 * Signature: (I)Lmapper/Property;
 */
JNIEXPORT jobject JNICALL Java_mapper_Link_property__I
  (JNIEnv *, jobject, jint);

/*
 * Class:     mapper_Link
 * Method:    setProperty
 * Signature: (Ljava/lang/String;Lmapper/Value;Z)Lmapper/Link;
 */
JNIEXPORT jobject JNICALL Java_mapper_Link_setProperty
  (JNIEnv *, jobject, jstring, jobject, jboolean);

/*
 * Class:     mapper_Link
 * Method:    removeProperty
 * Signature: (Ljava/lang/String;)Lmapper/Link;
 */
JNIEXPORT jobject JNICALL Java_mapper_Link_removeProperty
  (JNIEnv *, jobject, jstring);

/*
 * Class:     mapper_Link
 * Method:    clearStagedProperties
 * Signature: ()Lmapper/Link;
 */
JNIEXPORT jobject JNICALL Java_mapper_Link_clearStagedProperties
  (JNIEnv *, jobject);

/*
 * Class:     mapper_Link
 * Method:    push
 * Signature: ()Lmapper/Link;
 */
JNIEXPORT jobject JNICALL Java_mapper_Link_push
  (JNIEnv *, jobject);

/*
 * Class:     mapper_Link
 * Method:    id
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_mapper_Link_id
  (JNIEnv *, jobject);

/*
 * Class:     mapper_Link
 * Method:    numMaps
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_mapper_Link_numMaps
  (JNIEnv *, jobject);

/*
 * Class:     mapper_Link
 * Method:    device
 * Signature: (I)Lmapper/Device;
 */
JNIEXPORT jobject JNICALL Java_mapper_Link_device
  (JNIEnv *, jobject, jint);

#ifdef __cplusplus
}
#endif
#endif
